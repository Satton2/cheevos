<?php
/**
 * Cheevos
 * Cheevos Class
 *
 * @package   Cheevos
 * @author    Cameron Chunn
 * @copyright (c) 2017 Curse Inc.
 * @license   GPL-2.0-or-later
 * @link      https://gitlab.com/hydrawiki/extensions/cheevos
 **/

namespace Cheevos;

use RedisCache;
use RedisException;
use User;

class Cheevos {
	/**
	 * Main Request cURL wrapper.
	 *
	 * @param string $type
	 * @param string $path
	 * @param array  $data
	 *
	 * @return void
	 */
	private static function request($type, $path, $data = []) {
		global $wgCheevosHost, $wgCheevosClientId;

		if (empty($wgCheevosHost)) {
			throw new CheevosException('$wgCheevosHost is not configured.');
		}
		if (empty($wgCheevosClientId)) {
			throw new CheevosException('$wgCheevosClientId is not configured.');
		}

		$host = $wgCheevosHost;
		$type = strtoupper($type);

		$url = "{$host}/{$path}";
		$headers = [
			'Accept: application/json',
			'Content-Type: application/json',
			'Client-ID: ' . $wgCheevosClientId
		];

		$ch = curl_init();
		curl_setopt_array(
			$ch,
			[
				CURLOPT_RETURNTRANSFER		=> 1,
				CURLOPT_URL					=> $url,
				CURLOPT_SSL_VERIFYHOST		=> false,
				CURLOPT_SSL_VERIFYPEER		=> false,
				CURLOPT_CUSTOMREQUEST		=> $type,
				CURLOPT_CONNECTTIMEOUT		=> 1,
				CURLOPT_TIMEOUT				=> 6,
				CURLOPT_ENCODING			=> 'gzip'
			]
		);
		if (in_array($type, ['DELETE', 'GET']) && !empty($data)) {
			$url = $url . "/?" . http_build_query($data);
		} else {
			$postData = json_encode($data);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
			$headers[] = 'Content-Length: ' . strlen($postData);
		}
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		curl_close($ch);
		$result = json_decode($result, true);

		return $result;
	}

	/**
	 * Wrapper for Request Function for GET method.
	 *
	 * @param string $path
	 * @param array  $data
	 *
	 * @return void
	 */
	private static function get($path, $data = []) {
		return self::request('GET', $path, $data);
	}

	/**
	 * Wrapper for Request Function for PUT method.
	 *
	 * @param string $path
	 * @param array  $data
	 *
	 * @return void
	 */
	private static function put($path, $data = []) {
		return self::request('PUT', $path, $data);
	}

	/**
	 * Wrapper for Request Function for POST method.
	 *
	 * @param string $path
	 * @param array  $data
	 *
	 * @return void
	 */
	private static function post($path, $data = []) {
		return self::request('POST', $path, $data);
	}

	/**
	 * Wrapper for Request Function for DELETE method.
	 *
	 * @param string $path
	 * @param array  $data
	 *
	 * @return void
	 */
	private static function delete($path, $data = []) {
		return self::request('DELETE', $path, $data);
	}

	/**
	 * Handle the return from a CURL request.
	 *
	 * @param array   $return   - Return from CURL request.
	 * @param string  $expected - Expected array key to return.
	 * @param string  $class    - Class to initialize with returned data.
	 * @param boolean $single   - Only return the first request of an initialized class.
	 *
	 * @return mixed
	 */
	private static function return($return, $expected = null, $class = null, $single = false) {
		// Throw Errors if we have API errors.
		if ($return === null || $return === false) {
			throw new CheevosException('Cheevos Service Unavailable', 503);
		}
		if (isset($return['code']) && $return['code'] !== 200) {
			throw new CheevosException($return['message'], $return['code']);
		}

		// Handles getting only the data we want
		if ($expected && isset($return[$expected])) {
			$return = $return[$expected];
		}

		// Return data as classes instead of arrays.
		if ($class && class_exists($class)) {
			$holder = [];
			foreach ($return as $classme) {
				if (is_array($classme)) {
					$object = new $class($classme);
					if ($object->hasId()) {
						$holder[$object->getId()] = $object;
					} else {
						$holder[] = $object;
					}
				}
				if ($single) {
					break;
				}
			}
			$return = $holder;

			// If we classify things, single will only return the first.
			if ($single) {
				reset($return);
				$return = current($return);
			}
		}
		return $return;
	}

	/**
	 * Validate data recieved from Cheevos
	 *
	 * @param array $body
	 *
	 * @return void
	 */
	private static function validateBody($body) {
		if (!is_array($body)) {
			$body = json_decode($body, 1);
			if (is_null($body)) {
				return false;
			} else {
				return $body;
			}
		} else {
			return $body;
		}
	}

	/**
	 * Invalid API Cache
	 *
	 * @return boolean	Success
	 */
	public static function invalidateCache() {
		global $wgRedisServers;

		$redis = RedisCache::getClient('cache');

		if ($redis === false) {
			return false;
		}

		$cache = false;
		$redisKey = 'cheevos:apicache:*';
		$prefix = isset($wgRedisServers['cache']['options']['prefix']) ? $wgRedisServers['cache']['options']['prefix'] : "";

		try {
			$cache = $redis->getKeys($redisKey);
			foreach ($cache as $key) {
				$key = str_replace($prefix . "cheevos", "cheevos", $key); // remove prefix if exists, because weird.
				$redis->del($key);
			}
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			return false;
		}

		return true;
	}

	/**
	 * Returns all relationships for a user by global id
	 *
	 * @param User $user
	 *
	 * @return array
	 */
	public static function getFriends(User $user): array {
		$globalId = self::getUserIdForService($user);
		$friendTypes = self::return(self::get("friends/{$globalId}"));
		if (is_array($friendTypes)) {
			foreach ($friendTypes as $category => $serviceUserIds) {
				if (is_array($serviceUserIds)) {
					foreach ($serviceUserIds as $key => $serviceUserId) {
						$user = self::getUserForServiceUserId($serviceUserId);
						if (!$user) {
							unset($friendTypes[$category][$key]);
						} else {
							$friendTypes[$category][$key] = $user;
						}
					}
				}
			}
		} else {
			$friendTypes = [];
		}

		return $friendTypes;
	}

	/**
	 * Return friendship status
	 *
	 * @param User $from
	 * @param User $to
	 *
	 * @return array
	 */
	public static function getFriendStatus(User $from, User $to) {
		$fromGlobalId = self::getUserIdForService($from);
		$toGlobalId = self::getUserIdForService($to);
		$return = self::get("friends/{$fromGlobalId}/{$toGlobalId}");
		return self::return($return);
	}

	/**
	 * Create a frienship request
	 *
	 * @param User $from
	 * @param User $to
	 *
	 * @return array
	 */
	public static function createFriendRequest(User $from, User $to) {
		$fromGlobalId = self::getUserIdForService($from);
		$toGlobalId = self::getUserIdForService($to);
		$return = self::put("friends/{$fromGlobalId}/{$toGlobalId}");
		return self::return($return);
	}

	/**
	 * Accept a friendship request (by creating a request the oposite direction!)
	 *
	 * @param User $from
	 * @param User $to
	 *
	 * @return array
	 */
	public static function acceptFriendRequest(User $from, User $to) {
		return self::createFriendRequest($from, $to);
	}

	/**
	 * Remove a friendship association between 2 users.
	 *
	 * @param User $from
	 * @param User $to
	 *
	 * @return array
	 */
	public static function removeFriend(User $from, User $to) {
		$fromGlobalId = self::getUserIdForService($from);
		$toGlobalId = self::getUserIdForService($to);
		$return = self::delete("friends/{$fromGlobalId}/{$toGlobalId}");
		return self::return($return);
	}

	/**
	 * Cancel friend request by removing assosiation.
	 *
	 * @param User $from
	 * @param User $to
	 *
	 * @return array
	 */
	public static function cancelFriendRequest(User $from, User $to) {
		return self::removeFriend($from, $to);
	}

	/**
	 * Get all achievements with caching.
	 *
	 * @param string $siteKey MD5 Hash Site Key
	 *
	 * @return mixed Ouput of self::return.
	 */
	public static function getAchievements($siteKey = null) {
		$redis = RedisCache::getClient('cache');
		$cache = false;
		$redisKey = 'cheevos:apicache:getAchievements:' . ($siteKey ? $siteKey : 'all');

		if ($redis !== false) {
			try {
				$cache = $redis->get($redisKey);
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		}

		$return = unserialize($cache, [false]);
		if (!$cache || !$return) {
			$return = self::get(
				'achievements/all',
				[
					'site_key' => $siteKey,
					'limit'	=> 0
				]
			);

			try {
				if ($redis !== false && isset($return['achievements'])) {
					$redis->setEx($redisKey, 300, serialize($return));
				}
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		}

		return self::return($return, 'achievements', '\Cheevos\CheevosAchievement');
	}

	/**
	 * Get achievement by database ID with caching.
	 *
	 * @param integer	Achievement ID
	 *
	 * @return mixed	Ouput of self::return.
	 */
	public static function getAchievement($id) {
		$redis = RedisCache::getClient('cache');
		$cache = false;
		$redisKey = 'cheevos:apicache:getAchievement:' . $id;

		if ($redis !== false) {
			try {
				$cache = $redis->get($redisKey);
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		}

		if (!$cache || !unserialize($cache)) {
			$return = self::get("achievement/{$id}");
			try {
				if ($redis !== false) {
					$redis->setEx($redisKey, 300, serialize($return));
				}
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		} else {
			$return = unserialize($cache);
		}

		$return = [$return]; // The return function expects an array of results.
		return self::return($return, 'achievements', '\Cheevos\CheevosAchievement', true);
	}

	/**
	 * Soft delete an achievement from the service.
	 *
	 * @param integer	Achievement ID
	 * @param integer	Global ID
	 *
	 * @return mixed	Array
	 */
	public static function deleteAchievement($id, $globalId) {
		$return = self::delete(
			"achievement/{$id}",
			[
				"author_id" => intval($globalId)
			]
		);
		return self::return($return);
	}

	/**
	 * PUT Achievement into Cheevos
	 *
	 * @param array   $body
	 * @param integer $id
	 *
	 * @return void
	 */
	private static function putAchievement($body, $id = null) {
		$body = self::validateBody($body);
		if (!$body) {
			return false;
		}

		$path = ($id) ? "achievement/{$id}" : "achievement";
		$return = self::put($path, $body);
		return self::return($return);
	}

	/**
	 * Update an existing achievement on the service.
	 *
	 * @param integer	Achievement ID
	 * @param array                    $body
	 *
	 * @return void
	 */
	public static function updateAchievement($id, $body) {
		return self::putAchievement($body, $id);
	}

	/**
	 * Create Achievement
	 *
	 * @param array $body
	 *
	 * @return void
	 */
	public static function createAchievement($body) {
		return self::putAchievement($body);
	}

	/**
	 * Get all categories.
	 *
	 * @acess public
	 * @param boolean	[Optional] Skip pulling data from the local cache.  Will still update the local cache.
	 *
	 * @return void
	 */
	public static function getCategories($skipCache = false) {
		$cache = false;
		$redis = RedisCache::getClient('cache');
		$redisKey = 'cheevos:apicache:getCategories';

		if (!$skipCache && $redis !== false) {
			try {
				$cache = $redis->get($redisKey);
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		}

		if (!$cache || !unserialize($cache)) {
			$return = self::get(
				'achievement_categories/all',
				[
					'limit'	=> 0
				]
			);
			try {
				if ($redis !== false) {
					$redis->setEx($redisKey, 300, serialize($return));
				}
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		} else {
			$return = unserialize($cache);
		}

		return self::return($return, 'categories', '\Cheevos\CheevosAchievementCategory');
	}

	/**
	 * Get Category by ID
	 *
	 * @param integer $id
	 *
	 * @return void
	 */
	public static function getCategory($id) {
		$redis = RedisCache::getClient('cache');
		$cache = false;
		$redisKey = 'cheevos:apicache:getCategory:' . $id;

		if ($redis !== false) {
			try {
				$cache = $redis->get($redisKey);
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		}

		if (!$cache || !unserialize($cache)) {
			$return = self::get("achievement_category/{$id}");
			try {
				if ($redis !== false) {
					$redis->setEx($redisKey, 300, serialize($return));
				}
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		} else {
			$return = unserialize($cache);
		}

		$return = [$return]; // return expect array of results. fake it.
		return self::return($return, 'categories', '\Cheevos\CheevosAchievementCategory', true);
	}

	/**
	 * Delete Category by ID (with optional user_id for user that deleted the category)
	 *
	 * @param integer $id
	 * @param integer $userId
	 *
	 * @return void
	 */
	public static function deleteCategory($id, $userId = 0) {
		$return = self::delete("achievement_category/{$id}", [
			"author_id" => $userId
		]);
		return self::return($return);
		;
	}

	/**
	 * Create a Category
	 *
	 * @param array   $body
	 * @param integer $id
	 *
	 * @return void
	 */
	private static function putCategory($body, $id = null) {
		$body = self::validateBody($body);
		if (!$body) {
			return false;
		}

		$path = ($id) ? "achievement_category/{$id}" : "achievement_category";
		$return = self::put($path, $body);
		return self::return($return);
	}

	/**
	 * Update Category by ID
	 *
	 * @param integer $id
	 * @param array   $body
	 *
	 * @return void
	 */
	public static function updateCategory($id, $body) {
		return self::putCategory($body, $id);
	}

	/**
	 * Create Category
	 *
	 * @param array $body
	 *
	 * @return void
	 */
	public static function createCategory($body) {
		return self::putCategory($body);
	}

	/**
	 * Call the increment end point on the API.
	 *
	 * @param array $body Post Request Body to be converted into JSON.
	 *
	 * @return mixed Array of return status including earned achievements or false on error.
	 */
	public static function increment($body) {
		$body = self::validateBody($body);
		if (!$body) {
			return false;
		}

		$return = self::post('increment', $body);

		return self::return($return);
	}

	/**
	 * Call increment to check for any unnotified achievement rewards.
	 *
	 * @param integer $globalId
	 * @param string  $siteKey
	 * @param boolean $forceRecalculate
	 *
	 * @return void
	 */
	public static function checkUnnotified($globalId, $siteKey, $forceRecalculate = false) {
		$globalId = intval($globalId);
		if (empty($globalId) || empty($siteKey)) {
			return;
		}

		$data = [
			'user_id' => $globalId,
			'site_key' => $siteKey,
			'recalculate' => $forceRecalculate,
			'deltas' => []
		];
		return self::increment($data);
	}

	/**
	 * Return StatProgress for selected filters.
	 *
	 * @param array $filters  Limit Filters - All filters are optional and can omitted from the array.
	 *                        This is an array since the amount of filter parameters is expected to be reasonably volatile over the life span of the product.
	 *                        This function does minimum validation of the filters.  For example, sending a numeric string when the service is expecting an integer will result in an exception being thrown.
	 *                        - $filters = [
	 *                        -     'user_id' => 0, //Limit by global user ID.
	 *                        -     'site_key' => 'example', //Limit by site key.
	 *                        -     'global' => false, //Set to true to aggregate stats from all sites.(Also causes site_key to be ignored.)
	 *                        -     'stat' => 'example', //Filter by a specific stat name.
	 *                        -     'sort_direction' => 'asc' or 'desc', //If supplied, the result will be sorted on the stats' count field.
	 *                        -     'start_time' => 'example', //If supplied, only stat deltas after this unix timestamp are considered.
	 *                        -     'end_time' => 'example', //If supplied, only stat deltas before this unix timestamp are considered.
	 *                        -     'limit' => 200, //Maximum number of results.  Defaults to 200.
	 *                        -     'offset' => 0, //Offset to start from the beginning of the result set.
	 *                        - ];
	 * @param User|null $user Filter by user.  Overwrites 'user_id' in $filters if provided.
	 *
	 * @return mixed
	 */
	public static function getStatProgress(array $filters = [], ?User $user = null) {
		if ($user !== null) {
			$filters['user_id'] = self::getUserIdForService($user);
		}

		foreach (['user_id', 'start_time', 'end_time', 'limit', 'offset'] as $key) {
			if (isset($filter[$key]) && !is_int($filter[$key])) {
				$filter[$key] = intval($filter[$key]);
			}
		}
		$filters['limit'] = (isset($filters['limit']) ? $filters['limit'] : 200);

		$return = self::get('stats', $filters);

		return self::return($return, 'stats', '\Cheevos\CheevosStatProgress');
	}

	/**
	 * Return WikiPointLog for selected filters.
	 *
	 * @param array	$filters  Limit Filters - All filters are optional and can omitted from the array.
	 *                        This is an array since the amount of filter parameters is expected to be reasonably volatile over the life span of the product.
	 *                        This function does minimum validation of the filters.  For example, sending a numeric string when the service is expecting an integer will result in an exception being thrown.
	 *                        - $filters = [
	 *                        -     'user_id' => 0, //Limit by global user ID.
	 *                        -     'site_key' => 'example', //Limit by site key.
	 *                        -     'limit' => 200, //Maximum number of results.  Defaults to 200.
	 *                        -     'offset' => 0, //Offset to start from the beginning of the result set.
	 *                        - ];
	 * @param User|null $user Filter by user.  Overwrites 'user_id' in $filters if provided.
	 *
	 * @return mixed
	 */
	public static function getWikiPointLog(array $filters = [], ?User $user = null) {
		if ($user !== null) {
			$filters['user_id'] = self::getUserIdForService($user);
		}

		foreach (['user_id', 'limit', 'offset'] as $key) {
			if (isset($filter[$key]) && !is_int($filter[$key])) {
				$filter[$key] = intval($filter[$key]);
			}
		}
		$filters['limit'] = (isset($filters['limit']) ? $filters['limit'] : 25);

		$return = self::get('points/user', $filters);

		return self::return($return, 'points', '\Cheevos\CheevosWikiPointLog');
	}

	/**
	 * Return stats/user_site_count for selected filters.
	 *
	 * @param User        $user    Global User ID
	 * @param string|null $siteKey [Optional] Filter by site key.
	 *
	 * @return mixed
	 */
	public static function getUserPointRank(User $user, ?string $siteKey = null) {
		$return = self::get(
			'points/user_rank',
			[
				'user_id'	=> self::getUserIdForService($user),
				'site_key'	=> $siteKey
			]
		);

		return self::return($return, 'rank');
	}

	/**
	 * Return StatDailyCount for selected filters.
	 *
	 * @param array	$filters Limit Filters - All filters are optional and can omitted from the array.
	 *                       This is an array since the amount of filter parameters is expected to be reasonably volatile over the life span of the product.
	 *                       This function does minimum validation of the filters.  For example, sending a numeric string when the service is expecting an integer will result in an exception being thrown.
	 *                       - $filters = [
	 *                       -     'site_key' => 'example', //Limit by site key.
	 *                       -     'stat' => 'example', //Filter by a specific stat name.
	 *                       -     'limit' => 200, //Maximum number of results.  Defaults to 200.
	 *                       -     'offset' => 0, //Offset to start from the beginning of the result set.
	 *                       - ];
	 *
	 * @return mixed
	 */
	public static function getStatDailyCount(array $filters = []) {
		foreach (['limit', 'offset'] as $key) {
			if (isset($filter[$key]) && !is_int($filter[$key])) {
				$filter[$key] = intval($filter[$key]);
			}
		}
		$filters['limit'] = (isset($filters['limit']) ? $filters['limit'] : 200);

		$return = self::get('stats/daily', $filters);

		return self::return($return, 'stats', '\Cheevos\CheevosStatDailyCount');
	}

	/**
	 * Return StatMonthlyCount for selected filters.
	 *
	 * @param array     $filter Limit Filters - All filters are optional and can omitted from the array.
	 *                          This is an array since the amount of filter parameters is expected to be
	 *                          reasonably volatile over the life span of the product. This function
	 *                          does minimum validation of the filters.  For example, sending a numeric
	 *                          string when the service is expecting an integer will result in an
	 *                          exception being thrown.
	 * 		                    - $filters = [
	 *                          -     'user_id' => 1, //Limit by service user ID.
	 *                          -     'site_key' => 'example', //Limit by site key.
	 *                          -     'stat' => 'example', //Filter by a specific stat name.
	 *                          -     'limit' => 200, //Maximum number of results.  Defaults to 200.
	 *                          -     'offset' => 0, //Offset to start from the beginning of the result set.
	 * 		                    - ];
	 * @param User|null $user   Filter by user.  Overwrites 'user_id' in $filters if provided.
	 *
	 * @return mixed
	 */
	public static function getStatMonthlyCount(array $filters = [], ?User $user = null) {
		if ($user !== null) {
			$filters['user_id'] = self::getUserIdForService($user);
		}

		foreach (['user_id', 'limit', 'offset'] as $key) {
			if (isset($filter[$key]) && !is_int($filter[$key])) {
				$filter[$key] = intval($filter[$key]);
			}
		}
		$filters['limit'] = (isset($filters['limit']) ? $filters['limit'] : 200);

		$return = self::get('stats/monthly', $filters);

		return self::return($return, 'stats', '\Cheevos\CheevosStatMonthlyCount');
	}

	/**
	 * Return stats/user_site_count for selected filters.
	 *
	 * @param User   $user User
	 * @param string $stat Filter by stat name (Example: article_edit to get number of Wikis Edited)
	 *
	 * @return mixed
	 */
	public static function getUserSitesCountByStat(User $user, string $stat) {
		$return = self::get(
			'stats/user_sites_count',
			[
				'user_id'	=> self::getUserIdForService($user),
				'stat'		=> $stat
			]
		);

		return self::return($return, 'count');
	}

	/**
	 * Get achievement status for an user.
	 *
	 * @param integer	Global User ID
	 * @param string	Site Key - From DynamicSettings
	 *
	 * @return mixed
	 */
	public static function getAchievementStatus($globalId, $siteKey = null) {
		$return = self::get(
			'achievements/status',
			[
				'limit'	=> 0,
				'user_id' => intval($globalId),
				'site_key' => $siteKey
			]
		);

		return self::return($return, 'status', '\Cheevos\CheevosAchievementStatus');
	}

	/**
	 * Return AchievementProgress for selected filters.
	 *
	 * @param array     $filters Limit Filters - All filters are optional and can omitted from the array.
	 *                           - $filters = [
	 *                           -     'site_key' => 'example', //Limit by site key.
	 *                           -     'achievement_id' => 0, //Limit by achievement ID.
	 *                           -     'user_id' => 0, //Limit by global user ID.
	 *                           -     'category_id' => 0, //Limit by category ID.
	 *                           -     'earned' => false, //Only get progress for earned achievements.
	 *                           -     'limit' => 100, //Maximum number of results.
	 *                           -     'offset' => 0, //Offset to start from the beginning of the result set.
	 *                           - ];
	 * @param User|null $user    Filter by user.  Overwrites 'user_id' in $filters if provided.
	 *
	 * @return mixed
	 */
	public static function getAchievementProgress(array $filters = [], ?User $user = null) {
		if ($user !== null) {
			$filters['user_id'] = self::getUserIdForService($user);
		}

		foreach (['user_id', 'achievement_id', 'category_id', 'limit', 'offset'] as $key) {
			if (isset($filter[$key]) && !is_int($filter[$key])) {
				$filter[$key] = intval($filter[$key]);
			}
		}

		$return = self::get('achievements/progress', $filters);

		return self::return($return, 'progress', '\Cheevos\CheevosAchievementProgress');
	}

	/**
	 * Get progress for an achievement
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public static function getProgressCount($site_key = null, $achievement_id = null) {
		$return = self::get("achievements/progress/count", [
			"achievement_id" => $achievement_id,
			"site_key"	=> $site_key
		]); // return expect array of results. fake it.
		return self::return($return);
	}

	public static function getProgressTop($site_key = null, $ignore_users = [], $achievement_id = null, $limit = 1) {
		$return = self::get("achievements/progress/top", [
			"ignore_users" => implode(",", $ignore_users),
			"site_key"	=> $site_key,
			"achievement_id" => $achievement_id,
			"limit"	=> $limit
		]); // return expect array of results. fake it.
		return self::return($return);
	}

	/**
	 * Get process for achievement
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public static function getProgress($id) {
		$return = [self::get("achievements/progress/{$id}")]; // return expect array of results. fake it.
		return self::return($return, 'progress', '\Cheevos\CheevosAchievementProgress', true);
	}

	/**
	 * Delete progress towards an achievement.
	 *
	 * @param integer	Progress ID
	 *
	 * @return mixed
	 */
	public static function deleteProgress($id) {
		$return = self::delete("achievements/progress/{$id}");
		return self::return($return);
	}

	/**
	 * Put process for achievement. Either create or updates.
	 *
	 * @param array   $body
	 * @param integer $id
	 *
	 * @return void
	 */
	public static function putProgress($body, $id = null) {
		$body = self::validateBody($body);
		if (!$body) {
			return false;
		}

		$path = ($id) ? "achievements/progress/{$id}" : "achievements/progress";
		$return = self::put($path, $body);
		return self::return($return);
	}

	/**
	 * Update progress
	 *
	 * @param integer $id
	 * @param array   $body
	 *
	 * @return void
	 */
	public static function updateProgress($id, $body) {
		return self::putProgress($body, $id);
	}

	/**
	 * Create Progress
	 *
	 * @param array $body
	 *
	 * @return void
	 */
	public static function createProgress($body) {
		return self::putProgress($body);
	}

	/**
	 * Return user_options/{id} for selected filters.
	 *
	 * @param integer	Global User ID
	 *
	 * @return mixed
	 */
	public static function getUserOptions($globalId) {
		$redis = RedisCache::getClient('cache');
		$cache = false;
		$redisKey = 'cheevos:apicache:useroptions:' . $globalId;

		if ($redis !== false) {
			try {
				$cache = $redis->get($redisKey);
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		}

		if (!$cache || !unserialize($cache)) {
			$return = self::get('user_options/' . intval($globalId));
			try {
				if ($redis !== false) {
					$redis->setEx($redisKey, 86400, serialize($return));
				}
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		} else {
			$return = unserialize($cache);
		}

		return self::return($return, 'useroptions');
	}

	/**
	 * Put user options up to Cheevos.
	 *
	 * @param integer	Global User ID
	 * @param array	POST Body
	 *
	 * @return mixed
	 */
	public static function setUserOptions($body) {
		$body = self::validateBody($body);
		if (!$body) {
			return false;
		}

		$redis = RedisCache::getClient('cache');
		$redisKey = 'cheevos:apicache:useroptions:' . $body['user_id'];
		try {
			if ($redis !== false) {
				$redis->del($redisKey);
			}
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}

		$path = "user_options/" . $body['user_id'];
		$return = self::put($path, $body);
		return self::return($return);
	}

	/**
	 * Get all points promotions with caching.
	 *
	 * @param string	[Optional] Site Key
	 * @param boolean	[Optional] Skip Cache Look Up.
	 *
	 * @return mixed	Ouput of self::return.
	 */
	public static function getPointsPromotions($siteKey = null, $skipCache = false) {
		$redis = RedisCache::getClient('cache');
		$cache = false;
		$redisKey = 'cheevos:apicache:getPointsPromotions:' . ($siteKey ? $siteKey : 'all');

		if (!$skipCache && $redis !== false) {
			try {
				$cache = $redis->get($redisKey);
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
			$return = unserialize($cache, [false]);
		}

		if (!$cache || !$return) {
			$return = self::get(
				'points/promotions',
				[
					'site_key' => $siteKey,
					'limit'	=> 0
				]
			);

			try {
				if ($redis !== false && isset($return['promotions'])) {
					$redis->setEx($redisKey, 300, serialize($return));
				}
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		}

		return self::return($return, 'promotions', '\Cheevos\CheevosSiteEditPointsPromotion');
	}

	/**
	 * Get points promotion by database ID with caching.
	 *
	 * @param integer	SiteEditPointsPromotion ID
	 *
	 * @return mixed	Ouput of self::return.
	 */
	public static function getPointsPromotion($id) {
		$redis = RedisCache::getClient('cache');
		$cache = false;
		$redisKey = 'cheevos:apicache:getPointsPromotion:' . $id;

		if ($redis !== false) {
			try {
				$cache = $redis->get($redisKey);
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		}

		if (!$cache || !unserialize($cache)) {
			$return = self::get("points/promotions/{$id}");
			try {
				if ($redis !== false) {
					$redis->setEx($redisKey, 300, serialize($return));
				}
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		} else {
			$return = unserialize($cache);
		}

		$return = [$return]; // The return function expects an array of results.
		return self::return($return, 'promotions', '\Cheevos\CheevosSiteEditPointsPromotion', true);
	}

	/**
	 * Soft delete an points promotion from the service.
	 *
	 * @param integer	SiteEditPointsPromotion ID
	 * @param integer	Global ID
	 *
	 * @return mixed	Array
	 */
	public static function deletePointsPromotion($id) {
		$redis = RedisCache::getClient('cache');
		$redisKey = 'cheevos:apicache:getPointsPromotion:' . $id;

		if ($redis !== false) {
			try {
				$redis->del($redisKey);
			} catch (RedisException $e) {
				wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
			}
		}

		$return = self::delete(
			"points/promotions/{$id}"
		);
		return self::return($return);
	}

	/**
	 * PUT PointsPromotion into Cheevos
	 *
	 * @param array   $body
	 * @param integer $id
	 *
	 * @return mixed	Output of self::return.
	 */
	public static function putPointsPromotion($body, $id = null) {
		$id = intval($id);
		$body = self::validateBody($body);
		if (!$body) {
			return false;
		}

		$path = ($id > 0 ? "points/promotions/{$id}" : "points/promotions");
		$return = self::put($path, $body);

		if ($id > 0) {
			$redis = RedisCache::getClient('cache');
			$redisKey = 'cheevos:apicache:getPointsPromotion:' . $id;
			if ($redis !== false) {
				try {
					$redis->del($redisKey);
				} catch (RedisException $e) {
					wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
				}
			}
		}

		return self::return($return);
	}

	/**
	 * Update an existing points promotion on the service.
	 *
	 * @param integer	SiteEditPointsPromotion ID
	 * @param array                                $body
	 *
	 * @return void
	 */
	public static function updatePointsPromotion($id, $body) {
		return self::putPointsPromotion($body, $id);
	}

	/**
	 * Create PointsPromotion
	 *
	 * @param array $body
	 *
	 * @return void
	 */
	public static function createPointsPromotion($body) {
		return self::putPointsPromotion($body);
	}

	/**
	 * Revokes edit points for the provided revision IDs related to the page ID.
	 *
	 * @param integer	Page ID
	 * @param array	Revision IDs
	 * @param string	Site Key
	 *
	 * @return mixed	Array
	 */
	public static function revokeEditPoints($pageId, $revisionIds, $siteKey) {
		$revisionIds = array_map('intval', $revisionIds);
		$return = self::post(
			"points/revoke_revisions",
			[
				'page_id'		=> intval($pageId),
				'revision_ids'	=> $revisionIds,
				'site_key'		=> $siteKey
			]
		);
		return self::return($return);
	}

	/**
	 * Get the user ID for this user in the Cheevos service.
	 *
	 * @param User $user
	 *
	 * @return integer
	 */
	public static function getUserIdForService(User $user): int {
		return $user->getId();
	}

	/**
	 * Get a local User object for this user ID in the Cheevos service.
	 *
	 * @param integer $serviceUserId
	 *
	 * @return User|null
	 */
	public static function getUserForServiceUserId(int $serviceUserId): ?User {
		return User::newFromId($serviceUserId);
	}
}
